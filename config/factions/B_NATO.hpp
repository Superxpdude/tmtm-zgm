// BLUFOR
// NATO
// Loadouts are below
class B_NATO
{
	class base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {"", "", "", "", {}, {}, ""};
		secondaryWeapon[] = {"", "", "", "", {}, {}, ""};
		handgunWeapon[] = {"hgun_P07_F", "", "", "", {"16Rnd_9x21_Mag",17}, {}, ""};
		binocular = "Binocular";
		
		uniformClass = "U_B_CombatUniform_mcam";
		headgearClass = "H_HelmetB";
		facewearClass = "G_Tactical_Clear";
		vestClass = "V_PlateCarrier1_rgr";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap", "ItemMicroDAGR", "TFAR_anprc152", "ItemCompass", "ItemWatch", "NVGoggles"};
		
		uniformItems[] = {{"ItemcTabHCam",1},{"ACE_EntrenchingTool",1},{"16Rnd_9x21_Mag",1,17}};
		vestItems[] = {};
		backpackItems[] = {};
		
		basicMedUniform[] = {{"ACE_fieldDressing",10},{"ACE_epinephrine",2},{"ACE_morphine",4}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class B_Soldier_F: base
	{
		displayName = "Rifleman";
		
		primaryWeapon[] = {"arifle_MX_F", "", "acc_pointer_IR", "optic_Hamr", {"30Rnd_65x39_caseless_mag_Tracer",30}, {}, ""};
		vestItems[] = {{"30Rnd_65x39_caseless_mag_Tracer",7,30},{"SmokeShell",4,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1},{"MiniGrenade",2,1}};
	};
	
	class B_Soldier_A_F: base
	{
		displayName = "Ammo Bearer";
		
		primaryWeapon[] = {"arifle_MX_F", "", "acc_pointer_IR", "optic_Hamr", {"30Rnd_65x39_caseless_mag_Tracer",30}, {}, ""};
		
		headgearClass = "H_HelmetB_grass";
		backpackClass = "B_Carryall_mcamo";
		
		vestItems[] = {{"30Rnd_65x39_caseless_mag_Tracer",7,30},{"SmokeShell",4,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1},{"MiniGrenade",2,1}};
		backpackItems[] = {{"30Rnd_65x39_caseless_mag_Tracer",15,30},{"100Rnd_65x39_caseless_mag_Tracer",3,100},{"20Rnd_762x51_Mag",3,20},{"MiniGrenade",3,1},{"SmokeShell",10,1}};
	};
	
	class B_Soldier_LAT_F: base
	{
		displayName = "Rifleman (AT)";
		
		primaryWeapon[] = {"arifle_MX_F", "", "acc_pointer_IR", "optic_Hamr", {"30Rnd_65x39_caseless_mag_Tracer",30}, {}, ""};
		secondaryWeapon[] = {"launch_MRAWS_sand_F", "", "", "", {"MRAWS_HEAT_F",1}, {}, ""};
		
		headgearClass = "H_HelmetB_sand";
		vestClass = "V_PlateCarrier2_rgr";
		backpackClass = "B_AssaultPack_rgr";
		
		vestItems[] = {{"30Rnd_65x39_caseless_mag_Tracer",7,30},{"SmokeShell",4,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1},{"MiniGrenade",2,1}};
		backpackItems[] = {{"MRAWS_HEAT_F",2,1},{"MRAWS_HE_F",1,1}};
	};
	
	class B_Soldier_AR_F
	{
		displayName = "Autorifleman";
		
		primaryWeapon[] = {"arifle_MX_SW_F", "", "acc_pointer_IR", "optic_Hamr", {"100Rnd_65x39_caseless_mag_Tracer",100}, {}, "bipod_01_F_snd"};
		
		uniformClass = "U_B_CombatUniform_mcam_tshirt";
		headgearClass = "H_HelmetB_grass";
		vestClass = "V_PlateCarrier2_rgr";

		vestItems[] = {{"100Rnd_65x39_caseless_mag_Tracer",4,100},{"SmokeShell",4,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1},{"MiniGrenade",1,1}};
	};
};