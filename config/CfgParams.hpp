// Mission parameters
// Available on mission lobby
// https://community.bistudio.com/wiki/Arma_3_Mission_Parameters

// Uncomment this block, and replace the example param if you're going to use custom mission parameters
class header_mission
{
	title = "===== Mission Settings =====";
	values[] = {0};
	texts[] = {""};
	default = 0;
};
#ifdef SXP_ZEUS_BLUFOR
class zeus_faction_blufor
{
	title = "Player Faction: BLUFOR";
	values[] = {0};
	texts[] = {"NATO"};
	default = 0;
};
#endif
#ifdef SXP_ZEUS_OPFOR
class zeus_faction_opfor
{
	title = "Player Faction: OPFOR";
	values[] = {0};
	texts[] = {"CSAT"};
	default = 0;
};
#endif
#ifdef SXP_ZEUS_INDEP
class zeus_faction_independent
{
	title = "Player Faction: Independent";
	values[] = {0};
	texts[] = {"AAF"};
	default = 0;
};
#endif