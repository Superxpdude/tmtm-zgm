/*
	SXP_fnc_loadoutOnStart
	Author: Superxpdude
	Loads the correct inventory for the unit upon mission start.
	Information is pulled from the side faction set by the lobby parameter.
	This is a modified version of XPT_fnc_loadCurrentInventory from the XP template.
	All loadouts using this script should be in the new setUnitLoadout config format.
	
	Parameters:
		0: Object - Unit that needs a new loadout applied
		
	Returns: Nothing
*/

// Define our variables
params ["_unit"];
private ["_loadout"];

// Make sure the unit isn't a virtual entity
if (_unit isKindOf "VirtualMan_F") exitWith {};

// Check if the unit has a special loadout defined
_loadout = _unit getVariable ["loadout", nil];

// If no loadout is defined, grab the unit's classname as their loadout
if (isNil "_loadout") then {
	_loadout = typeOf _unit;
};

// Get the associated loadout for each side. Default values should always be the standard A3 factions
private _bluParam = ["zeus_faction_blufor", 0] call BIS_fnc_getParamValue;
private _opParam = ["zeus_faction_opfor", 0] call BIS_fnc_getParamValue;
private _indepParam = ["zeus_faction_independent", 0] call BIS_fnc_getParamValue;

// Convert the lobby parameter settings to strings
private _bluFaction = switch (_bluParam) do {
	case 0: {"B_NATO"};
	case 1: {"B_NATO_Pacific"}; // APEX DLC required
	case 2: {"B_CTRG"};
	case 3: {"B_CTRG_Pacific"}; // APEX DLC required
	case 4: {"B_FIA"};
};

private _opFaction = switch (_opParam) do {
	case 0: {"O_CSAT"};
	case 1: {"O_CSAT_Urban"};
	case 2: {"O_CSAT_Pacific"}; // APEX DLC required
	case 3: {"O_CSAT_Viper"}; // APEX DLC required
	case 4: {"O_CSAT_Viper_Pacific"}; // APEX DLC required
};

private _indepFaction = switch (_indepParam) do {
	case 0: {"I_AAF"};
	case 1: {"I_Syndikat"}; // APEX DLC required
};

// Grab the correct faction for the player
private _faction = switch (side player) do {
	case west: {_bluFaction};
	case east: {_opFaction};
	case independent: {_indepFaction};
};

// Load the correct loadout
if (isClass ((getMissionConfig "CfgXPT") >> "loadouts" >> _faction >> _loadout)) then {
	[_unit, (getMissionConfig "CfgXPT") >> "loadouts" >> _faction >> _loadout] call XPT_fnc_loadInventory;
} else {
	[true, format ["No loadout defined for unit. Loadout: '%1' Unit: '%2'", _loadout, name _unit], 2] call XPT_fnc_error;
};